'use strict';

var angular = require('angular');

module.exports = angular.module('gift-manager.gift-list.components', [
	require('./sidebar/services')
])

.directive('giftSidebar', require('./sidebar/sidebar.directive'))

.name;