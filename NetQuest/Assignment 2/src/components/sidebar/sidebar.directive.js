'use strict';

module.exports = SideBarMenuDrv;

/* @ngInject */
function SideBarMenuDrv() {
	var directive = {
		restrict: 'E',
		template: require('./sidebar.tpl.html'),
		scope: {},
		controller: SideBarDrvCtrl,
		controllerAs: 'vm',
		bindToController: {
		},
	};
	return directive;
}

/* @ngInject */
function SideBarDrvCtrl(sidebarMenuService) {
	var vm = this,
		previousSelected = 0;

	vm.sidebarMenuService = sidebarMenuService;
	vm.sidebarMenuService.tabSelected = 0;
	vm.showCategoryForm = false;

	vm.addCategory = addCategory;
	vm.select = select;
	vm.removeCategory = removeCategory;


	function addCategory() {
		var newObject = { name: vm.newCategory, selected: false };
		if ( vm.sidebarMenuService.categories.length === 0 ) {
			newObject.selected = true;
		}
		vm.sidebarMenuService.categories.push( newObject );
		vm.newCategory = '';
	}

	function select(index){
		vm.sidebarMenuService.categories[previousSelected].selected = false;
		vm.sidebarMenuService.categories[index].selected = true;
		vm.sidebarMenuService.tabSelected = index;
		previousSelected = index;
	}

	function removeCategory(index){
		vm.sidebarMenuService.categories.splice(index, 1);
		if ( vm.sidebarMenuService.categories.length === 1 ) {
			vm.sidebarMenuService.categories[0].selected = true;
		}
	}
}

