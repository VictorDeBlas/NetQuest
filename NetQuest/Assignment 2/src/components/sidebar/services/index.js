// index.js
'use strict';

var angular = require('angular');

module.exports = angular.module('gift-manager.sidebar-menu.services', [

])

//.factory('Product', require('./Product.factory'))
.factory('sidebarMenuService', require('./sidebarMenuService.factory'))

.name;