'use strict';

var angular = require('angular');

module.exports = angular.module('giftManager', [

	require('./gift-list'),
	require('./add-gift'),
	require('./components'),

	require('angular-route')
])

.config(function($routeProvider) {

	$routeProvider
		.when('/addGift', {
			template: require('./add-gift/gift.tpl.html'),
			controller:'AddGiftController',
			controllerAs: 'vm'
		})
		.when('/giftList',{
			template: require('./gift-list/gift-list.tpl.html'),
			controller: 'GiftListController',
			controllerAs: 'vm'
		})
		.otherwise({
			redirectTo: '/giftList'
		});
})

.name;
