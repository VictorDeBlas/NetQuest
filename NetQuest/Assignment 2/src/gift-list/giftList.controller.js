'use strict';

module.exports = GiftListController;

/* @ngInject */
function GiftListController($scope, addGiftService, sidebarMenuService) {
	var vm = this;

	vm.giftVisible = [];
	vm.sidebarMenuService = sidebarMenuService;

	activate();


	function activate(){
		attachWatcher();
	}

	function attachWatcher() {
		$scope.$watch('vm.sidebarMenuService.tabSelected', function(){
			if ( sidebarMenuService.categories.length > 0 ) {
				filterArray();
			}
		});
	}

	function filterArray(){

		var giftListLength = addGiftService.giftList.length,
			actual,
			currentCategory = sidebarMenuService.categories[sidebarMenuService.tabSelected].name;

		for ( var i = 0; i < giftListLength; i++ ) {
			actual = addGiftService.giftList[i];
			if ( actual.category === currentCategory ) {
				vm.giftVisible.push(actual);
			}
		}	
	}
}
