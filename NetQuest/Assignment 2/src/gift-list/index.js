'use strict';

var angular = require('angular');

module.exports = angular.module('gift-manager.gift-list', [

])

.controller('GiftListController', require('./giftList.controller'))

.name;