// index.js
'use strict';

var angular = require('angular');

module.exports = angular.module('gift-manager.add-gift.services', [

])

//.factory('Product', require('./Product.factory'))
.factory('addGiftService', require('./addGiftService.factory'))

.name;