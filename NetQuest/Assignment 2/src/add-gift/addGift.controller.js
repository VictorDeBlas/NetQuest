'use strict';

module.exports = AddGiftController;

/* @ngInject */
function AddGiftController(addGiftService, sidebarMenuService) {
	var vm = this;

	vm.giftForm = {};
	vm.sidebarMenuService = sidebarMenuService;

	vm.addGift = addGift;

	function addGift(){
		addGiftService.giftList.push(vm.giftForm);
		
		addGiftService.addGift(vm.giftForm)
			.then(function(){
				window.location.href = '#/giftList';
			});

		
	}
}
