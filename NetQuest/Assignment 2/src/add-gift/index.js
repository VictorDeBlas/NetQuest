'use strict';

var angular = require('angular');

module.exports = angular.module('gift-manager.add-gift', [
	require('./services'),
])
.controller('AddGiftController', require('./addGift.controller'))

.name;